
## GeoCoder
  
Quick console app to feed a csv to as first argument (; separated) with first two columns address data.
  
It will write out all data to same file name prefixed with output.csv and add 4 columns to the end of the row.
  
- Status of GoogleApi call, Should be OK
- address found by google for address in first two columns
- latitude
- longitude
  
All hard work is done by googlemaps Nuget package:
[Google Maps from Maxim @Github](https://github.com/maximn/google-maps)
  
Currently added a rate limit of 1 call per 200ms in. Google API seems to run a rate limit on calls per second.
  
Maximum request per day seems to be between 2k and 10k per IP. Or you could add  your Google API key somewhere. See googlemaps package from maxim.

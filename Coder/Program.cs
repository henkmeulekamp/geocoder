﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using CmdLine;
using Google.Kml;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Entities.Geocoding.Response;

namespace Coder
{
    class Program
    {
        private static List<Placemark> _placeMarks; 

        static void Main(string[] args)
        {
            CoderArguments arguments =null;
            try
            {
                arguments = CommandLine.Parse<CoderArguments>();
            }
            catch (CommandLineException exception)
            {
                Console.WriteLine(exception.ArgumentHelp.Message);
                Console.WriteLine(exception.ArgumentHelp.GetHelpText(Console.BufferWidth));                
            }

            if (arguments == null) return;

            if (!File.Exists(arguments.SourceCsv)) return;
            int lineCounter = 0;


            bool createKml = (!string.IsNullOrWhiteSpace(arguments.DestinationKml));

            if (createKml)
            {
                _placeMarks = new List<Placemark>();
            }

            using (var lineWriter = CreateDestinationFile(arguments))
            {
                foreach (var row in ReadCsvRows(arguments.SourceCsv))
                {
                    if(row.Length<=1) continue;

                    try
                    {
                        lineCounter++;
                        //get address part from column A; preferable streetname housenumber
                        var addressPart1 = row[0];
                        //get address part two from column b; zipcode, city and maybe country
                        var addressPart2 = row[1];
                        //todo make more dynamic, read from number of columns given by args
                        var address = string.Format("{0}, {1}", addressPart1, addressPart2);
                       
                        GeocodingResponse geocode = GeoCode(address);
                    
                        var location = geocode.Results.FirstOrDefault();

                        //copy inout csv rows into string array
                        var outPutRow = row.ToList();

                        //if we received valid location data, add it 
                        if (location != null
                            && location.Geometry != null
                            && location.Geometry.Location != null)
                        {
                            outPutRow.AddRange(
                                new[]
                                    {
                                        geocode.Status.ToString(),
                                        location.FormattedAddress ?? string.Empty,
                                        location.Geometry.Location.Latitude.ToString(CultureInfo.InvariantCulture),
                                        location.Geometry.Location.Longitude.ToString(CultureInfo.InvariantCulture)
                                    });
                            if (geocode.Status == Status.OK)
                            {
                                AddPlaceMark(address, row, location);
                            }
                        }
                        else
                        {
                            outPutRow.Add(geocode.Status.ToString());
                        }

                        //write string array as single line
                        lineWriter.WriteLine(string.Join(AppConst.CsvFieldSeparator,outPutRow));

                        //google maps delay : there seems to be a rate limit
                        Thread.Sleep(AppConst.GoogleRateLimitDelayMilliseconds);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error on line {0} {1}:{2}\nrow: {3}", lineCounter,e.Source, e.Message, row );
                    }
                }               
            }   
      
            if (createKml && _placeMarks.Any())
            {
                var document = new Document();
         
                foreach (var p in _placeMarks)
                {
                    document.Features.Add(p);
                }
                var kml = new Kml(document);
                XDocument xml = kml.ToKml();
               
                xml.Save(arguments.DestinationKml);

            }
        }

        private static void AddPlaceMark(string name, string[] row, Result result)
        {
            if (_placeMarks == null) return;

            var hasMoreData = row.Length > 2;

            var placemark = new Placemark
                {
                    name = name,
                    Geometry = new Point(new coordinates(result.Geometry.Location.Latitude,
                                                         result.Geometry.Location.Longitude)),
                    description =   !hasMoreData
                                    ? result.FormattedAddress
                                    : string.Format("{0}\n{1}\n",row[2], result.FormattedAddress),
                     
                };

            _placeMarks.Add(placemark);
        }

        private static StreamWriter CreateDestinationFile(CoderArguments arguments)
        {
            return File.CreateText(arguments.DestinationCsv
                                   ?? arguments.SourceCsv + AppConst.OutputCsvName);
        }

        private static GeocodingResponse GeoCode(string address)
        {
            try
            {
                var geocodeRequest = new GeocodingRequest
                {
                    Address = address,
 
                };
                return GoogleMaps.Geocode.Query(geocodeRequest);
            }
            catch (Exception e)
            {
                return new GeocodingResponse
                    {
                        Status = Status.INVALID_REQUEST,
                        Results = new List<Result>
                            {
                                new Result
                                    {
                                        FormattedAddress = string.Format("{0} : {1}", e.Source, e.Message)
                                    }
                            }
                    };
            }
        }

        private static IEnumerable<string[]> ReadCsvRows(string csvFile)
        {
            return from l in File.ReadLines(csvFile)
                   where l != null
                   let x = l.Split(AppConst.CsvFieldSeparator.ToCharArray(), StringSplitOptions.None)
                   select x;
        }
    }
}

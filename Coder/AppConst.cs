﻿namespace Coder
{
    public static class AppConst
    {
        /// <summary>
        /// csv field seperator
        /// </summary>
        public const string CsvFieldSeparator = ";";

        /// <summary>
        /// Appended to input file name when no output filename is given
        /// </summary>
        public const string OutputCsvName = "Output.csv";

        /// <summary>
        /// Google seems to have a rate limit for amount of queries per second on free accounts.
        /// </summary>
        public const int GoogleRateLimitDelayMilliseconds = 100;
    }
}
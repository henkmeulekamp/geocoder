﻿using CmdLine;

namespace Coder
{
    /// <summary>
    /// Ron Jacobs: http://code.msdn.microsoft.com/Command-Line-Parser-Library-a8ba828a
    /// </summary>
    [CommandLineArguments(Program = "GeoCoder", Title = "GeoCode Address data", 
        Description = "Lookup Lat/Lon data add to csv or exports as kml")]
    public class CoderArguments
    {
        [CommandLineParameter(Command = "?", Default = false, Description = "Show Help", Name = "Help", IsHelp = true)]
        public bool Help { get; set; }

        [CommandLineParameter(Name = "source", ParameterIndex = 1, Required = true,
            Description = "Input csv file, column 1 and 2 should have address data: streetname housenumber, city")]
        public string SourceCsv { get; set; }

        [CommandLineParameter(Name = "outputcsv", ParameterIndex = 2, 
            Description = "Output csv file, adds 4 columns, status, address, lat and lon")]
        public string DestinationCsv { get; set; }

        [CommandLineParameter(Name = "outputKml", ParameterIndex = 3,
            Description = "Output KML file")]
        public string DestinationKml { get; set; }
    }
}